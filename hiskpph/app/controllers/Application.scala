package controllers

import javax.inject.Inject

import controllers.webapi.UtilityService
import play.api._
import play.api.mvc._
import play.api.i18n.MessagesApi

class Application @Inject()(val messagesApi: MessagesApi) extends HisController with CookieAccessible {

  def index(lang: String, returnUrl: Option[String]) = Action {
    implicit val messages = {
      messagesApi.preferred(lang)
    }

    val countries = new UtilityService().getCountryCodes

    Ok(views.html.index(countries, returnUrl))
      .discardingCookies(makeDiscardingAuthCookie, makeDiscardingDataCookie, makeDiscardingSignalCookie)
  }

}
