package controllers

/**
 * Created by PB4N0091 on 9/28/2015.
 */

import controllers.json.Response
import play.api.libs.json.Json
import play.api.mvc.Controller

class HisController extends Controller with I18n {
  implicit val responseWriter = Json.writes[Response]

  protected def makeDefaultJsonResponse(status: Int, message: String) = {
    Json.toJson(Response(status, message))
  }
}
