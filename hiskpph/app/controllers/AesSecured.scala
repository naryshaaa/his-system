package controllers

import play.api.libs.Crypto


trait AesSecured {
  private val _privateKey = "@www.xyzauth.com"

  def encrypt(value: String) = Crypto.encryptAES(value, _privateKey)

  def decrypt(value: String) = Crypto.decryptAES(value, _privateKey)
}
