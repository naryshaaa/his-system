package controllers.json

/**
 * Created by PB4N0091 on 9/28/2015.
 */

case class Response(status: Int, message: String)
