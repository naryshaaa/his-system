package controllers

import play.api.i18n.Lang

trait I18n {
  private val _us_en = Tuple2("us_en", Lang("en-US"))
  private val _us_es = Tuple2("us_es", Lang("es-US"))
  private val _eu_en = Tuple2("eu_en", Lang("en-EU"))
  private val _eu_fr = Tuple2("eu_fr", Lang("fr-EU"))
  private val _eu_nl = Tuple2("eu_nl", Lang("nl-EU"))
  private val _eu_de = Tuple2("eu_de", Lang("de-EU"))
  private val _eu_es = Tuple2("eu_es", Lang("es-EU"))
  private val _eu_it = Tuple2("eu_it", Lang("it-EU"))
  private val _cn_zh_zn = Tuple2("cn_zh_cn", Lang("zh-CN"))
  private val _jp_ja = Tuple2("jp_ja", Lang("ja-JP"))
  private val _jp_en = Tuple2("jp_en", Lang("en-JP"))
  private val _tw_zh_tw = Tuple2("tw_zh_tw", Lang("zh-TW"))
  private val _kr_ko = Tuple2("kr_ko", Lang("ko-KR"))
  private val _la_es = Tuple2("la_es", Lang("es-LA"))
  private val _la_pt_br = Tuple2("la_pt_br", Lang("pt-LA"))
  private val _global_en = Tuple2("global_en", Lang("en-GL"))
  private val _global_es = Tuple2("global_es", Lang("es-GL"))
  private val _global_zh_hk = Tuple2("global_zh_hk", Lang("zh-GL"))
  private val _global_th = Tuple2("global_th", Lang("th-GL"))

  implicit def xyzLang2IsoLang(langCode: String) = xyz2Iso(langCode)

  def xyz2Iso(lang: String) : Seq[Lang] = {
    val isoLang = lang.toLowerCase match {
      case _us_en._1 => _us_en._2
      case _us_es._1 => _us_es._2
      case _eu_en._1 => _eu_en._2
      case _eu_fr._1 => _eu_fr._2
      case _eu_nl._1 => _eu_nl._2
      case _eu_de._1 => _eu_de._2
      case _eu_es._1 => _eu_es._2
      case _eu_it._1 => _eu_it._2
      case _cn_zh_zn._1 => _cn_zh_zn._2
      case _jp_ja._1 => _jp_ja._2
      case _jp_en._1 => _jp_en._2
      case _tw_zh_tw._1 => _tw_zh_tw._2
      case _kr_ko._1  => _kr_ko._2
      case _la_es._1 => _la_es._2
      case _la_pt_br._1 => _la_pt_br._2
      case _global_en._1 => _global_en._2
      case _global_es._1 => _global_es._2
      case _global_zh_hk._1 => _global_zh_hk._2
      case _global_th._1 => _global_th._2
      case _ => _us_en._2
    }

    Seq(isoLang)
  }

  def iso2Xyz(implicit lang: Lang): String = {
    lang match {
      case _us_en._2 => _us_en._1
      case _us_es._2 => _us_es._1
      case _eu_en._2 => _eu_en._1
      case _eu_fr._2 => _eu_fr._1
      case _eu_nl._2 => _eu_nl._1
      case _eu_de._2 => _eu_de._1
      case _eu_es._2 => _eu_es._1
      case _eu_it._2 => _eu_it._1
      case _cn_zh_zn._2 => _cn_zh_zn._1
      case _jp_ja._2 => _jp_ja._1
      case _jp_en._2 => _jp_en._1
      case _tw_zh_tw._2 => _tw_zh_tw._1
      case _kr_ko._2  => _kr_ko._1
      case _la_es._2 => _la_es._1
      case _la_pt_br._2 => _la_pt_br._1
      case _global_en._2 => _global_en._1
      case _global_es._2 => _global_es._1
      case _global_zh_hk._2 => _global_zh_hk._1
      case _global_th._2 => _global_th._1
      case _ => _us_en._1
    }
  }
}
