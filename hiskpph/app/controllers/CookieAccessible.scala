package controllers

import controllers.AesSecured
import play.api.Play
import play.api.mvc._

trait CookieAccessible extends AesSecured {
  private val _cookieTokenKey = "XAT"
  private val _cookieDataKey = "XAD"
  private val _cookieSignalKey = "XAS"
  private val _maxAge: Option[Int] = Play.current.configuration.getInt("cookie.maxAge")

  def makeAuthCookie(token: String, rememberMe: Boolean) =
    if(rememberMe) Cookie(_cookieTokenKey, encrypt(token), _maxAge) else Cookie(_cookieTokenKey, encrypt(token))

  def makeDataCookie(map: Map[String, String], rememberMe: Boolean) =
    if(rememberMe) Cookie(_cookieDataKey, generateDataEncryption(map), _maxAge) else Cookie(_cookieDataKey, generateDataEncryption(map))

  def makeSignalCookie(rememberMe: Boolean) =
    if(rememberMe) Cookie(_cookieSignalKey, "1", _maxAge, "/", None, false, false) else Cookie(_cookieSignalKey, "1", None, "/", None, false, false)

  def makeDiscardingAuthCookie = DiscardingCookie(_cookieTokenKey)

  def makeDiscardingDataCookie = DiscardingCookie(_cookieDataKey)

  def makeDiscardingSignalCookie = DiscardingCookie(_cookieSignalKey)

  def getAuthCookie(request: RequestHeader) =
    request.cookies.get(_cookieTokenKey)

  def getDataCookie(request: RequestHeader) =
    request.cookies.get(_cookieDataKey)

  private def generateDataEncryption(map: Map[String, String]) = encrypt(map.map { f: (String, String) => f._1 + "=" + f._2 }.mkString(":"))
}
