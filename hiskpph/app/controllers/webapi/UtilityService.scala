package controllers.webapi

import controllers.webapi.{Job, Industry, BaseXyzPrintingService}
import play.api.libs.json.Json

class UtilityService extends BaseXyzPrintingService {
  private val _getCountryCodeUrl = "/XYZPrinting/CountryCode/JSON"
  private val _getIndustryUrl = "/XYZPrinting/Industry/JSON"
  private val _getJobUrl = "/XYZPrinting/JobTitle/JSON"

  private implicit val _countryReader = Json.reads[Country]
  private implicit val _industryReader = Json.reads[Industry]
  private implicit val _jobReader = Json.reads[Job]

  def getCountryCodes = (httpGet(_getCountryCodeUrl).json \ "countryList").as[List[Country]]

  def getIndustries = (httpGet(_getIndustryUrl).json \ "industryList").as[List[Industry]]

  def getJobs = (httpGet(_getJobUrl).json \ "jobTitleList").as[List[Job]]
}
