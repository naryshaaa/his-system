package controllers.webapi

import controllers.webapi.BaseService
import play.api.Play
import play.api.libs.json.JsObject
import play.api.libs.ws._

import scala.concurrent._

abstract class BaseXyzPrintingService extends BaseService{
  private val _serviceDomain = Play.current.configuration.getString("webApi.domain").get
  private val _serviceAccessKey = Play.current.configuration.getString("webApi.accessKey").get

  protected override def httpGetAsync (url: String, headers: Map[String, String] = Map.empty, queryString: Map[String, String] = Map.empty) : Future[WSResponse] = {
    super.httpGetAsync(_serviceDomain + url, appendAccessKey(headers), queryString)
  }

  protected override def httpPostAsync (url: String, headers: Map[String, String] = Map.empty, parameters: Map[String, Seq[String]] = Map.empty) : Future[WSResponse] = {
    super.httpPostAsync(_serviceDomain + url, appendAccessKey(headers), parameters)
  }

  protected override def httpPostJsonAsync (url: String, headers: Map[String, String] = Map.empty, json: JsObject) : Future[WSResponse] = {
    super.httpPostJsonAsync(_serviceDomain + url, appendAccessKey(headers), json)
  }

  protected override def httpPutJsonAsync (url: String, headers: Map[String, String] = Map.empty, json: JsObject) : Future[WSResponse] = {
    super.httpPutJsonAsync(_serviceDomain + url, appendAccessKey(headers), json)
  }

  private def appendAccessKey(headers: Map[String, String]) = headers + ("key" -> _serviceAccessKey)
}
