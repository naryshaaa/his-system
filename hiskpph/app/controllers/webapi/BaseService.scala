package controllers.webapi

import play.api.Play.current
import play.api.libs.json.JsObject
import play.api.libs.ws._

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

abstract class BaseService {
  private val _awaitTimeOutDuration =  5.seconds

  protected def httpGet(url: String, headers: Map[String, String] = Map.empty, queryString: Map[String, String] = Map.empty) : WSResponse = {
    Await.result(httpGetAsync(url, headers, queryString), _awaitTimeOutDuration)
  }

  protected def httpGetAsync (url: String, headers: Map[String, String] = Map.empty, queryString: Map[String, String] = Map.empty) : Future[WSResponse] = {
    WS.url(url).withHeaders(headers.toSeq: _*).withQueryString(queryString.toSeq: _*).get()
  }

  protected def httpPost(url: String, headers: Map[String, String] = Map.empty, parameters: Map[String, Seq[String]] = Map.empty) : WSResponse = {
    Await.result(httpPostAsync(url, headers, parameters), _awaitTimeOutDuration)
  }

  protected def httpPostAsync (url: String, headers: Map[String, String] = Map.empty, parameters: Map[String, Seq[String]] = Map.empty) : Future[WSResponse] = {
    WS.url(url).withHeaders(headers.toSeq: _*).post(parameters)
  }

  protected def httpPostJson(url: String, headers: Map[String, String] = Map.empty, json: JsObject) : WSResponse = {
    Await.result(httpPostJsonAsync(url, headers, json), _awaitTimeOutDuration)
  }

  protected def httpPostJsonAsync(url: String, headers: Map[String, String] = Map.empty, json: JsObject) : Future[WSResponse] = {
    WS.url(url).withHeaders(headers.toSeq: _*).post(json)
  }

  protected def httpPutJson(url: String, headers: Map[String, String] = Map.empty, json: JsObject) : WSResponse = {
    Await.result(httpPutJsonAsync(url, headers, json), _awaitTimeOutDuration)
  }

  protected def httpPutJsonAsync(url: String, headers: Map[String, String] = Map.empty, json: JsObject) : Future[WSResponse] = {
    WS.url(url).withHeaders(headers.toSeq: _*).put(json)
  }

  protected def httpHead(url: String, headers: Map[String, String] = Map.empty) : WSResponse = {
    Await.result(httpHeadAsync(url, headers), _awaitTimeOutDuration)
  }

  protected def httpHeadAsync(url: String, headers: Map[String, String] = Map.empty) : Future[WSResponse] = {
    WS.url(url).withHeaders(headers.toSeq: _*).head()
  }
}
